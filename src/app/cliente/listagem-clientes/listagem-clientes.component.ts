import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Cliente} from '../../dominio/cliente';

@Component({
  selector: 'app-listagem-clientes',
  templateUrl: './listagem-clientes.component.html',
  styleUrls: ['./listagem-clientes.component.css']
})
export class ListagemClientesComponent implements OnInit {

  @Input() clientes: Cliente[];

  @Output('clienteSelecionado') clienteSelecionadoEmitter: EventEmitter<Cliente> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  emiteClienteSelecionado(cliente: Cliente) {
    this.clienteSelecionadoEmitter.emit(cliente);
  }

}
