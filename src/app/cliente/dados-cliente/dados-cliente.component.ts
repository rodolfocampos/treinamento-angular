import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { Cliente } from '../../dominio/cliente';

@Component({
  selector: 'app-dados-cliente',
  templateUrl: './dados-cliente.component.html',
  styleUrls: ['./dados-cliente.component.css']
})
export class DadosClienteComponent implements OnInit {

  @Input() cliente: Cliente;

  @Output('nomeClicado') nomeClicadoEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  nomeClicado() {
    this.nomeClicadoEmitter.emit();
  }
}
