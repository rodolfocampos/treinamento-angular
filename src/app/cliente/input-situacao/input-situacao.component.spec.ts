import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSituacaoComponent } from './input-situacao.component';

describe('InputSituacaoComponent', () => {
  let component: InputSituacaoComponent;
  let fixture: ComponentFixture<InputSituacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputSituacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSituacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
