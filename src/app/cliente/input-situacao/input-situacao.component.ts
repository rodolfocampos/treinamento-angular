import {Component, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-input-situacao',
  templateUrl: './input-situacao.component.html',
  styleUrls: ['./input-situacao.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputSituacaoComponent,
      multi: true
    }
  ]
})

export class InputSituacaoComponent implements OnInit, ControlValueAccessor {

  private callOnChange: (val) => void;
  private callOnTouched: (val) => void;

  value: number;
  disabled: boolean;

  constructor() {
  }

  toggleSituacao() {
    if (this.value === 1) {
      this.value = 0;
    } else {
      this.value = 1;
    }

    this.callOnTouched(this.value);
    this.callOnChange(this.value);
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this.callOnChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.callOnTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj; // 0 - Ativo , 1 - Pendente
  }
}
