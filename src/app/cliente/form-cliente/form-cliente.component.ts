import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Cliente} from '../../dominio/cliente';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-form-cliente',
  templateUrl: './form-cliente.component.html',
  styleUrls: ['./form-cliente.component.css']
})
export class FormClienteComponent implements OnInit {

  private $cliente: Cliente;
  @Output() clienteSalvo: EventEmitter<Cliente> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  @Input() get cliente(): Cliente {
    return this.$cliente;
  }

  set cliente(cliente: Cliente) {
    this.$cliente = new Cliente(cliente.nome, cliente.cpf, cliente.situacao);
  }

  salvarCliente(form: NgForm) {
    if (form.valid) {
      this.clienteSalvo.emit(this.$cliente);
    } else {
      alert('Existem dados inválidos!');
    }
  }
}
