import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import {isNullOrUndefined} from 'util';

@Directive({
  selector: '[appCpf]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: CpfDirective,
    multi: true
  }]
})
export class CpfDirective implements Validator {

  constructor() {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    let value: string = control.value;

    if (isNullOrUndefined(value)) {
      return undefined;
    }

    value = value.replace(/\D/g, '');

    if (value.length === 11) {
      return undefined;
    }

    return {
      cpfInvalido: true
    };
  }


}
