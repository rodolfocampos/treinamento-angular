import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DadosClienteComponent} from './dados-cliente/dados-cliente.component';
import {CpfPipe} from './pipe/cpf.pipe';
import {ListagemClientesComponent} from './listagem-clientes/listagem-clientes.component';
import {FormClienteComponent} from './form-cliente/form-cliente.component';
import {FormsModule} from '@angular/forms';
import {UtilModule} from '../util/util.module';
import { InputSituacaoComponent } from './input-situacao/input-situacao.component';
import { CpfDirective } from './validadores/cpf.directive';


@NgModule({
  declarations: [DadosClienteComponent, CpfPipe, ListagemClientesComponent, FormClienteComponent, InputSituacaoComponent, CpfDirective],
  exports: [DadosClienteComponent, ListagemClientesComponent, FormClienteComponent],
  imports: [
    CommonModule,
    FormsModule,
    UtilModule
  ]
})
export class ClienteModule {
}
