import {Directive, Inject, Self, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appLog]'
})
export class LogDirective implements OnInit {

  constructor(
    @Self() private element: ElementRef
  ) { }

  ngOnInit() {
    console.log(this.element.nativeElement);
  }

}
