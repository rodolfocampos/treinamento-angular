import {Component, ContentChild, Host, Input, OnInit, ViewChild} from '@angular/core';
import {NgForm, NgModel} from '@angular/forms';

@Component({
  selector: 'app-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.css']
})
export class FormControlComponent implements OnInit {

  @Input() label;

  @ContentChild(NgModel, {static: false}) model: NgModel;

  constructor(
    @Host() public form: NgForm
  ) {
  }

  ngOnInit() {
  }

}
