import {Directive, HostListener} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Directive({
  selector: '[appMascara]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: MascaraDirective,
    multi: true
  }]
})
export class MascaraDirective implements ControlValueAccessor {

  private callOnChange: (val) => void;
  private callOnTouched: (val) => void;
  value: string;
  disable: boolean;

  registerOnChange(fn: any): void {
    this.callOnChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.callOnTouched = fn;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disable = isDisabled;
  }

  cpfMascara(cpf): string {
    cpf = cpf.replace(/\D/g, '');
    cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2');
    cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2');
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
    return cpf;
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    const valor = this.cpfMascara($event.target.value);
    $event.target.value = valor;
    this.callOnChange(valor);
    this.callOnTouched(valor);

  }

  @HostListener('blur', ['$event'])
  onBlur($event: any) {
    this.onKeyup($event);
  }
}


