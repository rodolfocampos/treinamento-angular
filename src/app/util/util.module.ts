import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogDirective} from './log.directive';
import {FormControlComponent} from './form-control/form-control.component';
import {MascaraDirective} from './mascara/mascara.directive';
import {MonetarioDirective} from './monetario/monetario.directive';


@NgModule({
  declarations: [LogDirective, FormControlComponent, MascaraDirective, MonetarioDirective],
  exports: [LogDirective, FormControlComponent, MascaraDirective, MonetarioDirective],
  imports: [
    CommonModule
  ]
})
export class UtilModule {
}
