import {Directive, HostListener} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Directive({
  selector: '[appMonetario]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: MonetarioDirective,
    multi: true
  }]
})
export class MonetarioDirective implements ControlValueAccessor {

  private onTouched: (val) => void;
  private onCharge: (val) => void;
  value: number;
  disable: boolean;

  constructor() {
  }

  registerOnChange(fn: any): void {
    this.onCharge = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disable = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    const valor = this.aplicarMascara($event.target.value);
    console.log(valor);

    $event.target.value = valor;
    this.onCharge(valor);
    this.onCharge(valor);

  }

  private aplicarMascara(value: any) {
    let v = value.replace(/\D/g, '');
    v = (v / 100).toFixed(2) + '';
    v = v.replace('.', ',');
    v = v.replace(/(\d)(\d{3})(\d{3}),/g, '$1.$2.$3,');
    v = v.replace(/(\d)(\d{3}),/g, '$1.$2,');
    return v;
  }
}
