import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Conta} from '../../dominio/conta';

@Component({
  selector: 'app-dados-conta',
  templateUrl: './dados-conta.component.html',
  styleUrls: ['./dados-conta.component.css']
})
export class DadosContaComponent implements OnInit {

  @Input() conta: Conta;

  @Output('numeroDaConta') numeroDaContaEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  emiteNumeroDaConta( conta: Conta ) {
    this.numeroDaContaEmitter.emit(conta);
  }

}
