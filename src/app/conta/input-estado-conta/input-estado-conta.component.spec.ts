import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputEstadoContaComponent } from './input-estado-conta.component';

describe('InputEstadoContaComponent', () => {
  let component: InputEstadoContaComponent;
  let fixture: ComponentFixture<InputEstadoContaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputEstadoContaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputEstadoContaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
