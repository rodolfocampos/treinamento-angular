import {Component, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-input-estado-conta',
  templateUrl: './input-estado-conta.component.html',
  styleUrls: ['./input-estado-conta.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: InputEstadoContaComponent,
    multi: true
  }]
})

export class InputEstadoContaComponent implements OnInit, ControlValueAccessor {

  value: number;
  disable: boolean;
  onCharge: (val) => void;
  onTouched: (val) => void;

  constructor() {
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this.onCharge = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disable = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  setEstadoConta(estadoDaConta: number) {
    this.value = estadoDaConta;
    this.onCharge(this.value);
    this.onTouched(this.value);
  }

}
