import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Conta} from '../../dominio/conta';
import {NgForm} from '@angular/forms';
import {Cliente} from '../../dominio/cliente';

@Component({
  selector: 'app-form-conta',
  templateUrl: './form-conta.component.html',
  styleUrls: ['./form-conta.component.css']
})
export class FormContaComponent implements OnInit {

  private $conta: Conta;

  @Output() salvaContaEmitter: EventEmitter<Conta> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  @Input() clientes: Cliente[];

  @Input() get conta(): Conta {
    return this.$conta;
  }


  set conta(conta: Conta) {
    this.$conta = new Conta(conta.agencia, conta.estado, conta.numero, conta.saldo, conta.tipo, conta.titular, conta.diaDeAniversario);
  }

  salvarConta(form: NgForm) {
    if (form.valid) {
      this.salvaContaEmitter.emit(this.$conta);
    } else {
      alert('Existem dados inválidos!');

    }
  }
}
