import {Component, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-input-tipo-conta',
  templateUrl: './input-tipo-conta.component.html',
  styleUrls: ['./input-tipo-conta.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputTipoContaComponent,
      multi: true
    }
  ]
})

export class InputTipoContaComponent implements OnInit, ControlValueAccessor {

  private onTouched: (val) => void;
  private onChange: (val) => void;
  value: number;
  disable: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disable = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  setTipoConta(tipoConta: number) {
    this.value = tipoConta;
    this.onTouched(this.value);
    this.onChange(this.value);
  }
}
