import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputTipoContaComponent } from './input-tipo-conta.component';

describe('InputTipoContaComponent', () => {
  let component: InputTipoContaComponent;
  let fixture: ComponentFixture<InputTipoContaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputTipoContaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTipoContaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
