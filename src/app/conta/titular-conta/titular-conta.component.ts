import {Component, Input, OnInit} from '@angular/core';
import {Cliente} from '../../dominio/cliente';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-titular-conta',
  templateUrl: './titular-conta.component.html',
  styleUrls: ['./titular-conta.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TitularContaComponent,
      multi: true
    }
  ]
})

export class TitularContaComponent implements OnInit, ControlValueAccessor {

  @Input() clientes: Cliente[];

  private $value: string;

  private onTouched: (val) => void;

  private onChange: (val) => void;

  constructor() {
  }

  ngOnInit() {
  }

  get value(): string {
    return this.$value;
  }

  set value(value: string) {
    this.$value = value;

    const cliente = this.clientes.find(cli => cli.cpf == value);
    this.onTouched(cliente);
    this.onChange(cliente);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: Cliente): void {
    this.$value = obj ? obj.cpf : null;
  }
}
