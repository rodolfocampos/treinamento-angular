import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitularContaComponent } from './titular-conta.component';

describe('TitularContaComponent', () => {
  let component: TitularContaComponent;
  let fixture: ComponentFixture<TitularContaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitularContaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitularContaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
