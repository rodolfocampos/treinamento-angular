import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Conta } from '../../dominio/conta';


@Component({
  selector: 'app-listagem-contas',
  templateUrl: './listagem-contas.component.html',
  styleUrls: ['./listagem-contas.component.css']
})
export class ListagemContasComponent implements OnInit {

  @Input() contas: Conta[];
  // tslint:disable-next-line:no-output-rename
  @Output('contaSelecionada') contaSelecionadaEmitter: EventEmitter<Conta> = new EventEmitter();

  constructor() { }

  ngOnInit() {


  }

  emiteContaSelecionada( conta: Conta ) {
    this.contaSelecionadaEmitter.emit(conta);
  }

}
