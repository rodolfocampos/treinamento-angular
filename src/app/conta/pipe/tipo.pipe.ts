import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tipo'
})
export class TipoPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {

    if (value == 1){
      return 'CORRENTE';
    } else  if (value == 2) {
      return 'POUPANÇA';
    }

    return null;
  }

}
