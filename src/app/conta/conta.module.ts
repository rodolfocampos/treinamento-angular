import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListagemContasComponent } from './listagem-contas/listagem-contas.component';
import { EstadoPipe } from './pipe/estado.pipe';
import { TipoPipe } from './pipe/tipo.pipe';
import { DadosContaComponent } from './dados-conta/dados-conta.component';
import { FormContaComponent } from './form-conta/form-conta.component';
import {FormsModule} from '@angular/forms';
import { InputTipoContaComponent } from './input-tipo-conta/input-tipo-conta.component';
import {UtilModule} from '../util/util.module';
import { TitularContaComponent } from './titular-conta/titular-conta.component';
import { InputEstadoContaComponent } from './input-estado-conta/input-estado-conta.component';



@NgModule({
  declarations: [
    ListagemContasComponent,
    EstadoPipe,
    TipoPipe,
    DadosContaComponent,
    FormContaComponent,
    InputTipoContaComponent,
    TitularContaComponent,
    InputEstadoContaComponent
  ],
  exports: [
    ListagemContasComponent,
    DadosContaComponent,
    FormContaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UtilModule
  ]
})
export class ContaModule { }
