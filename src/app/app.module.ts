import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {ClienteModule} from './cliente/cliente.module';
import {ContaModule} from './conta/conta.module';

import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';
import {UtilModule} from './util/util.module';
import {ServicoModule} from './servico/servico.module';

registerLocaleData(localePt, 'pt-br');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ClienteModule,
    ContaModule,
    UtilModule,
    ServicoModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt-br'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
