import {Injectable} from '@angular/core';
import {Cliente} from '../../dominio/cliente';

@Injectable()
export class ClienteService {

  constructor() {
  }

  buscaClientes(): Cliente[] {
    return [
      new Cliente(
        'Gabriel Silva',
        '01430346171',
        0
      ),
      new Cliente(
        'Bárbara Luana',
        '01234567890',
        1
      )
    ];
  }
}
