import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContaService} from './conta/conta.service';
import {ClienteService} from './cliente/cliente.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ContaService,
    ClienteService
  ]
})
export class ServicoModule {
}
