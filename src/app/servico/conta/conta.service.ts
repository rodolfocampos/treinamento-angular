import { Injectable } from '@angular/core';
import {Conta} from '../../dominio/conta';
import {Cliente} from '../../dominio/cliente';

@Injectable()
export class ContaService {

  constructor() { }

  buscaContas(): Conta[] {
    return [
      new Conta(123, 0, 123, 1000.0, 1, new Cliente(
        'Gabriel Silva',
        '01430346171',
        0
      ), 10),
      new Conta(333, 0, 333, 1000.0, 2, new Cliente(
        'Bárbara Luana',
        '01234567890',
        1
      ), 10),
      new Conta(321, 0, 321, 1000.0, 1, new Cliente(
        'Bárbara Luana',
        '01234567890',
        1
      ), 10)
    ];
  }
}
