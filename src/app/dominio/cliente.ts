

export class Cliente {
  nome: string;
  cpf: string;
  situacao: number;

  constructor(
    nome: string,
    cpf: string,
    situacao: number
  ) {
    this.nome = nome;
    this.cpf = cpf;
    this.situacao = situacao;
  }
}
