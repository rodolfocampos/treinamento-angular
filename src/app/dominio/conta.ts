import {Cliente} from './cliente';

export class Conta {

  constructor(
    public agencia?: number,
    public estado?: number,
    public numero?: number,
    public saldo?: number,
    public tipo?: number,
    public titular?: Cliente,
    public diaDeAniversario?: number
  ) {}

}
