import {Component, OnInit} from '@angular/core';
import {Cliente} from './dominio/cliente';
import {Conta} from './dominio/conta';
import {ContaService} from './servico/conta/conta.service';
import {ClienteService} from './servico/cliente/cliente.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  cliente: Cliente;
  conta: Conta;
  contas: Conta[];
  clientes: Cliente[];

  constructor(
    private contaService: ContaService,
    private clienteService: ClienteService
  ) {
  }

  ngOnInit() {
    this.contas = this.contaService.buscaContas();
    this.clientes = this.clienteService.buscaClientes();
  }

  apresentaMensagem() {
    window.alert('Clicou no Número!');
  }

  clienteSelecionado(cliente: Cliente) {
    this.cliente = cliente;
  }

  contaSelecionada(conta: Conta) {
    // window.alert('Conta selecionada' + conta.agencia + ' ' + conta.numero)
    this.conta = conta;
  }

  atualizaCliente(novoCliente: Cliente) {
    this.clientes.splice(
      this.clientes.indexOf(this.cliente), 1, novoCliente
    );
    this.cliente = null;
  }

  atualizaConta(novaConta: Conta) {
    const index = this.contas.indexOf(this.conta);

    if (index > 0) {
      this.contas.splice(
        this.contas.indexOf(this.conta), 1, novaConta
      );
    } else {
      this.contas.push(novaConta);
    }

    this.conta = null;
  }

  novaConta() {
    this.conta = new Conta();
  }
}
